Feature: Book Testing
    Ejecuta la creacion de un libro.

    Background: Ingresa a menu
        Given El usuario toca la opcion "Ajustes"
        When El usuario toca la opcion "Administrar libros"
        And El usuario toca el boton "+"
        And Ingresa a menu
        And El usuario toca la opcion "Ajustes"
        And El usuario toca la opcion "Administrar libros"
        Then El usuario debe ver el item "Libro 2"

    Scenario: Eventos aleatorios
        When El usuario ejecuta "10" eventos de forma aleatoria
