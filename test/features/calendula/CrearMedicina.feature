@CrearMedicina
Feature: Crear medicina
    El usuario crea medicina

    Scenario 1: Crear Medicina
        Given El usuario abre la app
        When El usuario hace clic en el botón "botonTerminarTutorial"
		And El usuario hace clic en el botón "botonMedicinas"
		And El usuario hace clic en el botón "botonAgregarMedicinas"
		And El usuario hace clic en el botón "noActivarBasedeDatos"
		And El usuario escribe "Acetaminofen" en el campo de texto "campoBuscarMedicinaNombre"
		And El usuario hace clic en el botón "botonAgregarMedicina"
		And El usuario selecciona "opcionPildoras" en la lista "listaPresentacionMedicina"
		And El usuario hace clic en el botón "botonAceptarMedicina"
        Then El usuario debe ver un elemento en la lista de medicinas con nombre "Acetaminofen"

