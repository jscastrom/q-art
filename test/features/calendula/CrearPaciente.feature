@CrearPaciente
Feature: Crear Paciente
    El usuario crea un paciente

    Scenario 1: Crear Paciente
        Given El usuario abre la app
        When El usuario hace clic en el botón "botonTerminarTutorial"
		And El usuario hace clic en el botón "botonMenu"
		And El usuario hace clic en el botón "Pacientes"
		And El usuario hace clic en el botón "botonAgregarPacientes"
		And El usuario escribe "Ana Lucia" en el campo de texto "campoNombrePaciente"
		And El usuario hace clic en el botón "botonEditarAvatar"
		And El usuario selecciona "opcionAvatarMujer3" en la lista "listaAvatares"
		And El usuario hace clic en el botón "botonAceptarPaciente"
        Then El usuario debe ver un elemento en la lista de pacientes con nombre "Ana Lucia"