@RandomTesting
Feature: Random Testing
    Inicia sesión en MantiBT y ejecuta eventos aleatorios.

    Background: Inicio de sesión
        Given El usuario navega en la página "paginaMantisBT"
        When El usuario escribe "admin" en el campo de texto "campoUsuario"
        And El usuario hace clic en el botón "botonInicarSesion"
        And El usuario escribe "admin" en el campo de texto "campoContrasenia"
        And El usuario hace clic en el botón "botonInicarSesion"
        Then El usuario debe ver la ventana con título "Mi Vista - MantisBT"

    Scenario: Eventos aleatorios
        When El usuario ejecuta "10" eventos de forma aleatoria

