@VerInicidencias
Feature: Ver incidencias
    Inicia sesión en MantiBT y reporta una nueva incidencia con los campos básicos.

    Background: Inicio de sesión
        Given El usuario navega en la página "paginaMantisBT"
        When El usuario escribe "admin" en el campo de texto "campoUsuario"
        And El usuario hace clic en el botón "botonInicarSesion"
        And El usuario escribe "admin" en el campo de texto "campoContrasenia"
        And El usuario hace clic en el botón "botonInicarSesion"
        Then El usuario debe ver la ventana con título "Mi Vista - MantisBT"

    Scenario: Imprimir informe de incidencias
        When El usuario hace clic en el elemento "botonVerInicidencias"
        And El usuario espera hasta que el elemento "tablaIncidencias" sea visible
        And El usuario hace clic en el elemento "botonImprimirInforme"
        And El usuario espera hasta que el elemento "tablaInformeIncidencias" sea visible
        And El usuario hace clic en el elemento "botonImprimirWord"
        And El usuario se desplaza a la página anterior
        And El usuario espera hasta que el elemento "menuUsuario" sea visible
        And El usuario hace clic en el elemento "menuUsuario"
        And El usuario hace clic en el elemento "botonSalir"
        And El usuario espera 1 segundos

    Scenario: Imprimir informe de incidencias seleccionadas
        When El usuario hace clic en el elemento "botonVerInicidencias"
        And El usuario espera hasta que el elemento "tablaIncidencias" sea visible
        And El usuario hace clic en el elemento "botonImprimirInforme"
        And El usuario espera hasta que el elemento "tablaInformeIncidencias" sea visible
        And El usuario hace clic en el elemento "checkbox1"
        And El usuario hace clic en el elemento "checkbox2"
        And El usuario hace clic en el elemento "botonSoloSeleccionados"
        And El usuario hace clic en el elemento "botonImprimirWord"
        And El usuario se desplaza a la página anterior
        And El usuario se desplaza a la página anterior
        And El usuario espera hasta que el elemento "menuUsuario" sea visible
        And El usuario hace clic en el elemento "menuUsuario"
        And El usuario hace clic en el elemento "botonSalir"
        And El usuario espera 1 segundos