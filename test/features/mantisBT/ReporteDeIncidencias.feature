@ReporteDeInicidencias
Feature: Reporte de incidencias
    Inicia sesión en MantiBT y reporta una nueva incidencia con los campos básicos.

    Background: Inicio de sesión
        Given El usuario navega en la página "paginaMantisBT"
        When El usuario escribe "admin" en el campo de texto "campoUsuario"
        And El usuario hace clic en el botón "botonInicarSesion"
        And El usuario escribe "admin" en el campo de texto "campoContrasenia"
        And El usuario hace clic en el botón "botonInicarSesion"
        Then El usuario debe ver la ventana con título "Mi Vista - MantisBT"

    Scenario Outline: Reporte y consulta de incidencia básica
        When El usuario hace clic en el elemento "botonReportarInicidencia"
        And El usuario escribe "<resumen>" en el campo de texto "campoResumen"
        And El usuario escribe "<descripcion>" en el campo de texto "campoDescripcion"
        And El usuario hace clic en el botón "botonEnviarIncidencia"
        Then El usuario debe ver un elemento con texto "<mensaje>"
        And El usuario espera hasta que el elemento "botonVerInicidenciaReportada" sea visible
        When El usuario hace clic en el elemento "botonVerInicidenciaReportada"
        Then El usuario espera hasta que el elemento "tablaDetalleIncidencia" sea visible
        And El elemento "columnaDescripcion" debe tener el texto "<descripcion>"

        Examples:
            | resumen            | descripcion            | mensaje            |
            | Este es el resumen | Esta es la descripción | Operación exitosa. |