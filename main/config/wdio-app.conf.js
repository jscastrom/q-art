/**
 * WebdriverIO config file to run tests on native mobile apps.
 * Config file helps us configure all the settings and setup environments 
 * to run our tests.
 */

const host = '0.0.0.0';   // default appium host
const port = 4723;        // default appium port

const waitforTimeout = 30 * 60000;
const commandTimeout = 30 * 60000;

exports.config = {
    debug: false,
    specs: [
        './test/features/**/*.feature',
    ],

    reporters: ['allure'],
    reporterOptions: {
        allure: {
          outputDir:   './reports/allure-report/results/',
          disableWebdriverStepsReporting: false,
          disableWebdriverScreenshotsReporting: false,
          useCucumberStepReporter: false,
        },
    },

    host: host,
    port: port,

    maxInstances: 1,

    capabilities: [
        {
            appiumVersion: '1.10.0',                 // Appium module version
            browserName: '',                        // browser name is empty for native apps
            platformName: 'Android',
            app: 'C:\\Users\\William\\Documents\\Proyectos\\q-art\\app\\Calendula_v2.5.11_apkpure.com.apk',          // Path to your native app
            appPackage: 'es.usc.citius.servando.calendula',             // Package name of the app
			appActivity: 'es.usc.citius.servando.calendula.activities.StartActivity', // App activity of the app
            platformVersion: '8.0',              // Android platform version of the device
            deviceName: 'Pixel2',              // device name of the mobile device
            waitforTimeout: waitforTimeout,
            commandTimeout: commandTimeout,
            newCommandTimeout: 30 * 60000,
        }
    ],

    services: ['selenium-standalone','appium'],
    appium: {
        waitStartTime: 6000,
        waitforTimeout: waitforTimeout,
        command: 'appium.cmd',
        logFileName: 'appium.log',
        args: {
            address: host,
            port: port,
            commandTimeout: commandTimeout,
            sessionOverride: true,
            debugLogSpacing: true
        }
    },

    /**
     * test configurations
     */
    logLevel: 'silent',
    coloredLogs: true,
    framework: 'cucumber',          // cucumber framework specified 
    cucumberOpts: {
        compiler: ['js:babel-core/register'],
        backtrace: true,
        failFast: false,
        timeout: 5 * 60 * 60000,
        require: ['./main/steps-app/*.js'],      // importing/requiring step definition files
		tagExpression: '@AbrirApp'
    },
	

    /**
     * hooks help us execute the repeatitive and common utilities 
     * of the project.
     */
	 
	
    onPrepare: function () {
        console.log('<<< NATIVE APP TESTS STARTED >>>');
    },

    afterScenario: function (scenario) {
        browser.screenshot();
     },

    onComplete: function () {
        console.log('<<< TESTING FINISHED >>>');
    }
};
  