import { Selector, LocalStorage, RandomEvent } from './utilities.js';
import { selectorFile, localStorageFolder } from './constants.js';

export const localStorage = new LocalStorage(localStorageFolder);

export const selector = new Selector(selectorFile);