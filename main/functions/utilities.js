export class Selector {

    constructor(selectorFile) {
        this.selector = this.readProperties(selectorFile);
    }

    readProperties(file) {
        var fs = require('fs');
        var data = fs.readFileSync(file, 'utf8');
        return JSON.parse(data);
    }
    
    getSelector(feature, key) {
        var value = this.selector[feature][key];
        if (value == null) {
            value = key;
        }
        return value;
    }
}

export class LocalStorage {

    constructor(localStorageFolder) {
        if (typeof this.localStorage === "undefined" || this.localStorage === null) {
            var NodeLocalStorage = require('node-localstorage').LocalStorage;
            this.localStorage = new NodeLocalStorage(localStorageFolder);
        }
    }

    setItem(item, value) {
        return this.localStorage.setItem(item, value);
    }

    getItem(item) {
        return this.localStorage.getItem(item);
    }
}