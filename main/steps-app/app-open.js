import { Then } from 'cucumber';

var localStorage = require('../functions/app.js').localStorage;
var selector = require('../functions/app.js').selector;

Then(/^El usuario abre la app $/, function () {
    browser.element("//android.widget.ImageButton[@content-desc='Back']");
});