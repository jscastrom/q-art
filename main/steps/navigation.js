import { Given, When } from 'cucumber';

var localStorage = require('../functions/app.js').localStorage;
var selector = require('../functions/app.js').selector;

Given(/^El usuario navega en la página "([^"]*)"$/, function (keyPath) {
    var currentFeatureTag = localStorage.getItem('currentFeatureTag');
    var path = selector.getSelector(currentFeatureTag, keyPath);

    browser.url(path);
    browser.pause(1000);

    //throw 'I should see all green when the argument is "example"';
});

When(/^El usuario se desplaza a la página anterior$/, function () {
    browser.back();
    browser.pause(1000);

    //throw 'I should see all green when the argument is "example"';
});