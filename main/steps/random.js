import { When } from 'cucumber';

var localStorage = require('../functions/app.js').localStorage;
var selector = require('../functions/app.js').selector;

When(/^El usuario ejecuta "([^"]*)" eventos de forma aleatoria$/, function (events) {
    var currentFeatureTag = localStorage.getItem('currentFeatureTag');
    events = selector.getSelector(currentFeatureTag, events);

    randomEvent(events);
});

function randomEvent(events) {

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  };

  function getRandomText() {
    var alphabet = "abcdefghijklmnopqrstuvwxyz1234567890'[]*¨;:_!#$%&/()=?¡";
    var digits = getRandomInt(1, 255);
    var text = "";
    for (var i = 0; i < digits; i++) {
      text += alphabet.charAt(getRandomInt(0, digits - 1));
    }
    return text;
  };

  var monkeysLeft = events;

  if (monkeysLeft > 0) {
    var rndEvent = getRandomInt(0, 3);

    // Link
    if (rndEvent == 0) {
      var links = $$('a');
      if (links.length > 0) {
        var link = links[getRandomInt(0, links.length)];
        if (link.isVisible() && link.isEnabled()) {
          link.click();
          monkeysLeft = monkeysLeft - 1;
        };
      }
      browser.pause(1000);
      randomEvent(monkeysLeft);
    } 
    
    // Input Text
    else if (rndEvent == 1) {
      var inputs = $$('input[type="text"]');
      if (inputs.length > 0) {
        var input = inputs[getRandomInt(0, inputs.length)];
        if (input.isVisible() && input.isEnabled()) {
          input.clearElement();
          input.setValue(getRandomText());
          monkeysLeft = monkeysLeft - 1;
        };
      }
      browser.pause(1000);
      randomEvent(monkeysLeft);
    } 
    
    // Select
    else if (rndEvent == 2) {
      var selects = $$('select');
      if (selects.length > 0) {
        var select = selects[getRandomInt(0, selects.length)];
        if (select.isVisible() && select.isEnabled()) {
          var options = select.$$('option');
          var optionValue = options[getRandomInt(0, options.length)].getAttribute("value");
          select.selectByValue(optionValue);
          monkeysLeft = monkeysLeft - 1;
        };
      }
      browser.pause(1000);
      randomEvent(monkeysLeft);
    } 
    
    // Button
    else if (rndEvent == 3) {
      var buttons = $$('button');
      if (buttons.length > 0) {
        var button = buttons[getRandomInt(0, buttons.length)];
        if (button.isVisible() && button.isEnabled()) {
          button.click();
          monkeysLeft = monkeysLeft - 1;
        };
      }
      browser.pause(1000);
      randomEvent(monkeysLeft);
    }
  }
}