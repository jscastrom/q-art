import { Then } from 'cucumber';

var localStorage = require('../functions/app.js').localStorage;
var selector = require('../functions/app.js').selector;

Then(/^El usuario debe ver la ventana con título "([^"]*)"$/, function (titlePage) {
    browser.getTitle().should.equal(titlePage);
});

Then(/^El usuario debe ver un elemento con texto "([^"]*)"$/, function (text) {
    var element = browser.element("//*[contains(.,'" + text + "')]");
});

