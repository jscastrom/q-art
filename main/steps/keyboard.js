import { When } from 'cucumber';

var localStorage = require('../functions/app.js').localStorage;
var selector = require('../functions/app.js').selector;
var defaultSecondsWait = require('../functions/constants.js').defaultSecondsWait;

When(/^El usuario escribe "([^"]*)" en el campo de texto "([^"]*)"$/, function (text, keyElement) {
    var currentFeatureTag = localStorage.getItem('currentFeatureTag');
    keyElement = selector.getSelector(currentFeatureTag, keyElement);

    var element = browser.element(keyElement);
    element.waitForVisible(defaultSecondsWait * 1000);
    element.clearElement();
    element.setValue(text);
});